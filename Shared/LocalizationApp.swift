//
//  LocalizationApp.swift
//  Shared
//
//  Created by Vladislav Zhukov on 19.03.21.
//
//

import SwiftUI

@main
struct LocalizationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
