//
//  ContentView.swift
//  Shared
//
//  Created by Vladislav Zhukov on 19.03.21.
//
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text(NSLocalizedString("first_screen_title", comment: " "))
                .padding()
            
            Button(action: {}) {
                Text(NSLocalizedString("first_screen_button", comment: " "))
                    .padding()
            }
            
            Button(action: {}) {
                Text(NSLocalizedString("second_screen_button", comment: " "))
                    .padding()
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
